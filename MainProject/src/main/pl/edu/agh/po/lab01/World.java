
package pl.edu.agh.po.lab01;

import agh.cs.lab1.Direction;

import java.util.Arrays;

public class World
{
    public static void main(String[] args)
    {
        System.out.println("Start");

        Direction[] directions =
                Arrays.stream(args).
                filter(World::isValidDirection).
                map(x -> Direction.valueOf(x)).
                toArray(Direction[]::new);

        run(directions);

        System.out.println("Stop");
    }

    public static boolean isValidDirection(String arg)
    {
        try
        {
            Direction.valueOf(arg);
            return true;
        }catch (IllegalArgumentException e)
        {
            return false;
        }
    }

    private static void run(Direction[] args)
    {
        for(Direction d:args)
        {
            switch (d)
            {
                case f -> System.out.println("Zwierzak idzie do przodu");
                case b -> System.out.println("Zwierzak idzie do tylu");
                case r -> System.out.println("Zwierzak idzie w prawo");
                case l -> System.out.println("Zwierzak idzie w lewo");
            }
        }
    }
}
