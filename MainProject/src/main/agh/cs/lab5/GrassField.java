package agh.cs.lab5;

import agh.cs.lab2.Vector2d;
import agh.cs.lab6.WorldMapField;

import java.util.*;

import java.lang.Math;

public class GrassField extends AbstractWorldMap {
    private final Random randomGenerator;
    private final Vector2d topRightGenerator;
    private final Vector2d bottomLeftGenerator;

    public GrassField(int grassCount) {
        super();
        topRightGenerator = new Vector2d((int)Math.sqrt(grassCount*10), (int)Math.sqrt(grassCount*10));
        bottomLeftGenerator = new Vector2d(0, 0);
        randomGenerator = new Random();
        generateGrass(grassCount);
    }

    public GrassField(Random generator, int grassCount) {
        super();
        topRightGenerator = new Vector2d((int)Math.sqrt(grassCount*10), (int)Math.sqrt(grassCount*10));
        bottomLeftGenerator = new Vector2d(0, 0);
        randomGenerator = generator;
        generateGrass(grassCount);
    }

    protected void generateGrass(int grassCount)
    {
        for(int i = 0; i < grassCount; i++){
            Vector2d randomPos = new Vector2d(
                    bottomLeftGenerator.x + randomGenerator.nextInt(topRightGenerator.x - bottomLeftGenerator.x),
                    bottomLeftGenerator.y + randomGenerator.nextInt(topRightGenerator.y - bottomLeftGenerator.y));
            if(isOccupied(randomPos))
                i--;
            else {
                Grass grass = new Grass(randomPos);
                mapFields.put(randomPos, new WorldMapField(grass));
                mapBoundary.place(grass);
            }
        }
    }
}
