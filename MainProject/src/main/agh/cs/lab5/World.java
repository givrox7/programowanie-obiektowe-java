package agh.cs.lab5;

import agh.cs.lab2.MoveDirection;
import agh.cs.lab2.Vector2d;
import agh.cs.lab3.Animal;
import agh.cs.lab3.OptionsParser;
import pl.edu.agh.po.lab04.IWorldMap;

import java.util.List;

public class World {
    public static void main(String[] args) {
        try {
            List<MoveDirection> directions = OptionsParser.parse(args);
            IWorldMap map = new GrassField(5);
            map.place(new Animal(map));
            map.place(new Animal(map, new Vector2d(3, 4)));
            map.run(directions);
            System.out.print(map);
        }catch(Exception exception) {
            System.out.println(exception.toString());
        }
    }
}
