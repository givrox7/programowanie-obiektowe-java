package agh.cs.lab5;

import agh.cs.lab2.MoveDirection;
import agh.cs.lab2.Vector2d;
import agh.cs.lab3.Animal;
import agh.cs.lab6.IMapElement;
import agh.cs.lab6.WorldMapField;
import agh.cs.lab7.IPositionChangeObserver;
import agh.cs.lab7.MapBoundary;
import pl.edu.agh.po.lab04.IWorldMap;
import pl.edu.agh.po.lab04.MapVisualiser;

import java.util.*;

public abstract class AbstractWorldMap implements IWorldMap, IPositionChangeObserver {
    protected MapBoundary mapBoundary;
    protected final List<Animal> animals;
    protected final Map<Vector2d, WorldMapField> mapFields;
    protected final MapVisualiser mapVisualiser;

    protected AbstractWorldMap() {
        mapBoundary = new MapBoundary();
        mapVisualiser = new MapVisualiser(this);
        animals = new LinkedList<>();
        mapFields = new HashMap<>();
    }

    @Override
    public boolean canMoveTo(Vector2d position) {
        return !(mapFields.containsKey(position) && mapFields.get(position).isObstacle());
    }

    @Override
    public boolean place(Animal animal) {
        Vector2d position = animal.getPosition();
        if(mapFields.containsKey(position) && mapFields.get(position).isObstacle())
            throw new IllegalArgumentException("Can't place animal on " + position.toString());

        animals.add(animal);
        if(!mapFields.containsKey(position))
            mapFields.put(position, new WorldMapField());
        mapFields.get(position).place(animal);
        animal.addObserver(this);
        mapBoundary.place(animal);
        return true;
    }

    protected void removeFromField(Vector2d position, IMapElement element) {
        mapFields.get(position).pop(element);
        if(mapFields.get(position).top().isEmpty())
            mapFields.remove(position);
    }

    protected void placeOnField(Vector2d position, IMapElement element){
        if(!mapFields.containsKey(position))
            mapFields.put(position, new WorldMapField());
        mapFields.get(position).place(element);
    }

    @Override
    public void run(List<MoveDirection> directions) {
        if(animals.isEmpty())
            return;

        ListIterator<Animal> animalIterator = animals.listIterator();
        for (MoveDirection d : directions){
            if(!animalIterator.hasNext())
                animalIterator = animals.listIterator();

            Animal animal = animalIterator.next();
            animal.move(d);
        }
    }

    @Override
    public boolean isOccupied(Vector2d position) {
        return mapFields.containsKey(position);
    }

    @Override
    public Optional<IMapElement> objectAt(Vector2d position) {
        if(mapFields.containsKey(position))
            return mapFields.get(position).top();

        return Optional.empty();
    }

    @Override
    public String toString() {
        return mapVisualiser.draw(mapBoundary.getBottomLeft(), mapBoundary.getTopRight());
    }

    @Override
    public void positionChanged(IMapElement movedElement, Vector2d oldPosition, Vector2d newPosition) {
        removeFromField(oldPosition, movedElement);
        placeOnField(newPosition, movedElement);
    }
}
