package agh.cs.lab7;

import agh.cs.lab2.Vector2d;
import agh.cs.lab3.Animal;
import agh.cs.lab6.IMapElement;
import agh.cs.lab6.WorldMapField;

import java.util.*;

public class MapBoundary implements IPositionChangeObserver {
    private final TreeMap<List<Integer>,IMapElement> OX;
    private final TreeMap<List<Integer>,IMapElement> OY;

    public static int comparator(List<Integer> o1, List<Integer> o2)
    {
        int size = Math.min(o1.size(), o2.size());
        for(int i =0; i <size; i++){
            if(!o1.get(i).equals(o2.get(i)))
                return Integer.compare(o1.get(i), o2.get(i));
        }
        return Integer.compare(o1.size(), o2.size());
    }

    public MapBoundary() {
        OX = new TreeMap<>(MapBoundary::comparator);
        OY = new TreeMap<>(MapBoundary::comparator);
    }

    public Vector2d getBottomLeft() {
        return OX.isEmpty() || OY.isEmpty() ? new Vector2d(0,0) : new Vector2d(OX.get(OX.firstKey()).getPosition().x, OY.get(OY.firstKey()).getPosition().y);
    }

    public Vector2d getTopRight() {
        return OX.isEmpty() || OY.isEmpty() ? new Vector2d(0,0) : new Vector2d(OX.get(OX.lastKey()).getPosition().x, OY.get(OY.lastKey()).getPosition().y);
    }

    public void place(Animal animal) {
        animal.addObserver(this);
        place((IMapElement) animal);
    }

    private List<Integer> getXKey(Vector2d keyPosition, Class<?extends IMapElement> keyClass){
        return new ArrayList<>(Arrays.asList(keyPosition.x,
                keyPosition.y,
                -WorldMapField.getViewOrder().get(keyClass)));
    }

    private List<Integer> getYKey(Vector2d keyPosition, Class<?extends IMapElement> keyClass){
        return new ArrayList<>(Arrays.asList(keyPosition.y,
                keyPosition.x,
                -WorldMapField.getViewOrder().get(keyClass)));
    }

    public void place(IMapElement mapElement) {
        OX.put(getXKey(mapElement.getPosition(), mapElement.getClass()), mapElement);
        OY.put(getYKey(mapElement.getPosition(), mapElement.getClass()), mapElement);
    }

    public void remove(Animal animal) {
        animal.removeObserver(this);
        remove((IMapElement) animal);
    }

    public void remove(IMapElement mapElement) {
        OX.remove(getXKey(mapElement.getPosition(), mapElement.getClass()));
        OY.remove(getYKey(mapElement.getPosition(), mapElement.getClass()));
    }

    @Override
    public void positionChanged(IMapElement movedElement, Vector2d oldPosition, Vector2d newPosition) {
        OX.remove(getXKey(oldPosition, movedElement.getClass()));
        OY.remove(getYKey(oldPosition, movedElement.getClass()));
        place(movedElement);
    }
}
