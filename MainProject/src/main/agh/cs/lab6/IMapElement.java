package agh.cs.lab6;

import agh.cs.lab2.Vector2d;

public interface IMapElement {
    Vector2d getPosition();
    default boolean isObstacle(){return false;}
    String toString();

}
