package agh.cs.lab6;

import agh.cs.lab3.Animal;
import agh.cs.lab5.Grass;

import java.util.*;

public class WorldMapField {
    protected final static Map<Class<? extends IMapElement>, Integer> viewOrder = new HashMap<>();
    static{
        viewOrder.put(Animal.class, 1);
        viewOrder.put(Grass.class, 2);
    }

    public static Map<Class<? extends IMapElement>, Integer> getViewOrder() {
        return Collections.unmodifiableMap(viewOrder);
    }

    protected final List<IMapElement> mapElements;
    protected final List<IMapElement> obstacles;

    public WorldMapField() {
        mapElements = new LinkedList<>();
        obstacles = new LinkedList<>();
    }

    public WorldMapField(IMapElement element) {
        mapElements = new LinkedList<>();
        obstacles = new LinkedList<>();
        place(element);
    }

    private void placeOnMapElements(IMapElement element){
        ListIterator<IMapElement> iterator = mapElements.listIterator();
        while(iterator.hasNext()) {
            IMapElement e = iterator.next();
            if(viewOrder.get(element.getClass()) <= viewOrder.get(e.getClass()))
            {
                iterator.previous();
                break;
            }
        }
        iterator.add(element);
    }

    public void place(IMapElement element){
        placeOnMapElements(element);
        if(element.isObstacle())
            obstacles.add(element);
    }

    public boolean isObstacle()
    {
        return !obstacles.isEmpty();
    }

    public Optional<IMapElement> top()
    {
        return mapElements.isEmpty() ? Optional.empty() : Optional.of(mapElements.get(0));
    }

    public Optional<IMapElement> pop(IMapElement element){
        ListIterator<IMapElement> iterator = mapElements.listIterator();
        while(iterator.hasNext()) {
            IMapElement e = iterator.next();
            if(e == element)
            {
                iterator.remove();
                return Optional.of(element);
            }
        }

        return Optional.empty();
    }
}
