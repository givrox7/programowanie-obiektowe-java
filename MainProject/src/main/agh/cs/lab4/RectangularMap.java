package agh.cs.lab4;

import agh.cs.lab2.Vector2d;
import agh.cs.lab3.Animal;
import agh.cs.lab5.AbstractWorldMap;

public class RectangularMap extends AbstractWorldMap {
    protected Vector2d topRightCorner;
    protected Vector2d bottomLeftCorner;

    public RectangularMap(int width, int height) {
        super();
        topRightCorner = new Vector2d(width, height);
        bottomLeftCorner = new Vector2d(0,0);
    }

    @Override
    public boolean canMoveTo(Vector2d position) {
        if(!isInBorders(position))
            return false;

        return super.canMoveTo(position);
    }

    @Override
    public boolean place(Animal animal) {
        if(!isInBorders(animal.getPosition()))
            return false;

        return super.place(animal);
    }

    protected boolean isInBorders(Vector2d cell){
        return cell.precedes(topRightCorner) &&
                cell.follows(bottomLeftCorner);
    }
}
