package agh.cs.lab3;

import agh.cs.lab2.MapDirection;
import agh.cs.lab2.MoveDirection;
import agh.cs.lab2.Vector2d;
import agh.cs.lab6.IMapElement;
import agh.cs.lab7.IPositionChangeObserver;
import agh.cs.lab7.IPositionChangedPublisher;
import pl.edu.agh.po.lab04.IWorldMap;

import java.util.LinkedList;
import java.util.List;

public class Animal implements IMapElement, IPositionChangedPublisher {
    private MapDirection facing;
    private Vector2d position;
    private final IWorldMap map;
    protected final List<IPositionChangeObserver> positionChangeObservers;

    public Animal(IWorldMap map){
        facing = MapDirection.NORTH;
        position =  new Vector2d(2, 2);
        this.map = map;
        positionChangeObservers = new LinkedList<>();
    }

    public Animal(IWorldMap map, Vector2d initialPosition){
        facing = MapDirection.NORTH;
        position = initialPosition;
        this.map = map;
        positionChangeObservers = new LinkedList<>();
    }

    public Vector2d getPosition() {
        return position;
    }

    public void move(MoveDirection direction){
        switch (direction){
            case LEFT -> facing = facing.previous();
            case RIGHT -> facing = facing.next();
            case FORWARD -> changePositionIfPossible(facing.toUnitVector());
            case BACKWARD -> changePositionIfPossible(facing.toUnitVector().opposite());
        }
    }

    private void changePositionIfPossible(Vector2d vector)
    {
        Vector2d newPos = position.add(vector);
        if(map.canMoveTo(newPos)) {
            Vector2d oldPos = position;
            position = newPos;
            positionChanged(oldPos);
        }
    }


    @Override
    public String toString() {
        return switch (facing){
            case NORTH -> "^";
            case SOUTH -> "v";
            case WEST -> "<";
            case EAST -> ">";
        };
    }

    @Override
    public boolean isObstacle() {
        return true;
    }

    @Override
    public void addObserver(IPositionChangeObserver observer) {
        positionChangeObservers.add(observer);
    }

    @Override
    public void removeObserver(IPositionChangeObserver observer) {
        positionChangeObservers.remove(observer);
    }

    protected void positionChanged(Vector2d oldPosition)
    {
        for(IPositionChangeObserver observer : positionChangeObservers) {
            observer.positionChanged(this, oldPosition, position);
        }
    }
}