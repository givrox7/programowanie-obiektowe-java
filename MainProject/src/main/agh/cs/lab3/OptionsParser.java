package agh.cs.lab3;

import agh.cs.lab2.MoveDirection;

import java.util.LinkedList;
import java.util.List;

public class OptionsParser {
    public static List<MoveDirection> parse(String[] args){
        List<MoveDirection> sol = new LinkedList<>();
        for (String s: args) {
            try{
                sol.add(string2MoveDirection(s));
            }catch (IllegalArgumentException ignored){}
        }
        return sol;
    }

    private static MoveDirection string2MoveDirection(String str){
        if(str.equalsIgnoreCase("f") || str.equalsIgnoreCase("forward"))
            return MoveDirection.FORWARD;
        if(str.equalsIgnoreCase("b") || str.equalsIgnoreCase("backward"))
            return MoveDirection.BACKWARD;
        if(str.equalsIgnoreCase("l") || str.equalsIgnoreCase("left"))
            return MoveDirection.LEFT;
        if(str.equalsIgnoreCase("r") || str.equalsIgnoreCase("right"))
            return MoveDirection.RIGHT;
        throw new IllegalArgumentException(str + " is not legal move specification");
    }
}
