package agh.cs.lab3;

import agh.cs.lab2.MoveDirection;
import agh.cs.lab2.Vector2d;
import agh.cs.lab4.RectangularMap;
import pl.edu.agh.po.lab04.IWorldMap;

import java.util.List;

public class World {
    public static void main(String[] args){
        List<MoveDirection> directions = OptionsParser.parse(args);
        Animal animal = new Animal(new RectangularMap(4, 4));
        for (MoveDirection direction : directions) {
            animal.move(direction);
        }
        System.out.print(animal.getPosition() + animal.toString());
    }
}
