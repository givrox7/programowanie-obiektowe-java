package agh.cs.lab2;

public enum MapDirection {
    NORTH,
    SOUTH,
    WEST,
    EAST;

    private final static Vector2d vEast = new Vector2d(1,0);
    private final static Vector2d vWest = new Vector2d(-1,0);
    private final static Vector2d vNorth = new Vector2d(0,1);
    private final static Vector2d vSouth = new Vector2d(0,-1);
    @Override
    public String toString() {
        return switch (this){
            case EAST -> "Wschód";
            case WEST -> "Zachód";
            case NORTH -> "Północ";
            case SOUTH -> "Południe";
        };
    }

    public MapDirection next() {
        return switch (this) {
            case EAST -> SOUTH;
            case WEST -> NORTH;
            case NORTH -> EAST;
            case SOUTH -> WEST;
        };
    }

    public MapDirection previous(){
        return switch (this) {
            case EAST -> NORTH;
            case WEST -> SOUTH;
            case NORTH -> WEST;
            case SOUTH -> EAST;
        };
    }

    public Vector2d toUnitVector(){
        return switch (this) {
            case EAST -> vEast;
            case WEST -> vWest;
            case NORTH -> vNorth;
            case SOUTH -> vSouth;
        };
    }
}
