package agh.cs.lab4;

import agh.cs.lab2.MoveDirection;
import agh.cs.lab2.Vector2d;
import agh.cs.lab3.Animal;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.edu.agh.po.lab04.IWorldMap;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class WorldRectangularTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    void mainDefaultTest() {
        String[] in = {
                "b",
                "r",
                "f",
                "f",
        };

        World.main(in);

        IWorldMap map = new RectangularMap(10, 5);
        Animal animal1 = new Animal(map, new Vector2d(2,2));
        map.place(animal1);

        Animal animal2 = new Animal(map, new Vector2d(4,4));
        animal2.move(MoveDirection.RIGHT);
        map.place(animal2);

        assertEquals(map.toString(), outContent.toString());
    }

    @Test
    void mainDifferentDataTest() {
        String[] in = {
                "for",
                "forward",
                "b",
                "r",
                "d",
                "backward",
                "left",
                "f",
                "left",
                "b",
                "right"
        };

        World.main(in);

        IWorldMap map = new RectangularMap(10, 5);
        Animal animal1 = new Animal(map, new Vector2d(2,3));
        map.place(animal1);

        Animal animal2 = new Animal(map, new Vector2d(3,2));
        map.place(animal2);

        assertEquals(map.toString(), outContent.toString());
    }

    @Test
    void mainBorderTest() {
        String[] in = {
                "f",
                "b",
                "f",
                "b",
                "f",
                "b",
                "f",
                "b",
                "f",
                "b"
        };

        World.main(in);

        IWorldMap map = new RectangularMap(10, 5);
        Animal animal1 = new Animal(map, new Vector2d(2,5));
        map.place(animal1);

        Animal animal2 = new Animal(map, new Vector2d(3,0));
        map.place(animal2);

        assertEquals(map.toString(), outContent.toString());
    }

    @Test
    void mainIntegrationTest() {
        String[] in = {
                "fds",
                "dsa",
                "for",
                "forward",//1
                "b",//2
                "r",//1
                "r",//2
                "d",
                "backward",//1
                "left",//2
                "f",//1
                "left",//2
                "f",//1
                "f",//2
                "f",//1
                "right",//2
                "forward"//1
        };

        World.main(in);

        IWorldMap map = new RectangularMap(10, 5);
        Animal animal1 = new Animal(map, new Vector2d(2,3));
        animal1.move(MoveDirection.RIGHT);
        map.place(animal1);

        Animal animal2 = new Animal(map, new Vector2d(3,3));
        map.place(animal2);

        assertEquals(map.toString(), outContent.toString());
    }
}