package agh.cs.lab4;

import agh.cs.lab2.MoveDirection;
import agh.cs.lab2.Vector2d;
import agh.cs.lab3.Animal;
import agh.cs.lab5.AbstractWorldMapTest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class RectangularMapTest<T extends RectangularMap> extends AbstractWorldMapTest<T> {

    protected T instantiate(int width, int height) {
        return (T) new RectangularMap(width, height);
    }

    @BeforeEach
    public void instantiateMap() {
        map = instantiate(3, 4);
    }

    @Test
    void canMoveTo_EmptyOut() {
        assertFalse(map.canMoveTo(new Vector2d(2,5)));
    }

    @Test
    void canMoveTo_EmptyOut2() {
        assertFalse(map.canMoveTo(new Vector2d(2,-1)));
    }

    @Test
    protected void placeOut() {
        assertFalse(map.place(new Animal(map, new Vector2d(0,5))));
    }

    @Test
    protected void run1AnimalBorders() {
        Animal animal = new Animal(map, new Vector2d(2,2));
        map.place(animal);
        List<MoveDirection> directions = new LinkedList<>();

        directions.add(MoveDirection.FORWARD);
        directions.add(MoveDirection.RIGHT);
        directions.add(MoveDirection.FORWARD);
        directions.add(MoveDirection.FORWARD);
        directions.add(MoveDirection.FORWARD);

        map.run(directions);

        String expected = new Vector2d(3, 3).toString() + ">";
        assertEquals(expected, animal.getPosition() + animal.toString());
    }

    @Test
    void run2Animal() {
        Animal animal1 = new Animal(map, new Vector2d(2,2));
        Animal animal2 = new Animal(map, new Vector2d(1,2));
        map.place(animal1);
        map.place(animal2);
        List<MoveDirection> directions = new LinkedList<>();

        directions.add(MoveDirection.FORWARD);
        directions.add(MoveDirection.RIGHT);
        directions.add(MoveDirection.FORWARD);
        directions.add(MoveDirection.FORWARD);
        directions.add(MoveDirection.LEFT);
        directions.add(MoveDirection.LEFT);

        map.run(directions);

        String expected = new Vector2d(2, 4).toString() + "<" + new Vector2d(2, 2)+ "^";
        String actual = animal1.getPosition() + animal1.toString() + animal2.getPosition() + animal2.toString();
        assertEquals(expected, actual);
    }

    @Test
    void isOccupied_Empty() {
        assertFalse(map.isOccupied(new Vector2d(1,3)));
    }

    @Test
    void isOccupied_Out() {
        assertFalse(map.isOccupied(new Vector2d(1,-1)));
    }

    @Test
    void isOccupied_Occupied() {
        map.place(new Animal(map, new Vector2d(1, 3)));
        assertTrue(map.isOccupied(new Vector2d(1,3)));
    }

    @Test
    void objectAt_Empty() {
        assertSame(Optional.empty(), map.objectAt(new Vector2d(1,0)));
    }
}