package agh.cs.lab3;

import agh.cs.lab2.MoveDirection;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;

class OptionsParserTest {

    @Test
    void parseLettersTest() {
        String[] in = {
                "f",
                "b",
                "l",
                "r"
        };

        List<MoveDirection> expected = new LinkedList<>();
        expected.add(MoveDirection.FORWARD);
        expected.add(MoveDirection.BACKWARD);
        expected.add(MoveDirection.LEFT);
        expected.add(MoveDirection.RIGHT);

        List<MoveDirection> out = OptionsParser.parse(in);

        assertIterableEquals(expected, out);
    }

    @Test
    void parseWordsTest() {
        String[] in = {
                "forward",
                "backward",
                "left",
                "right"
        };

        List<MoveDirection> expected = new LinkedList<>();
        expected.add(MoveDirection.FORWARD);
        expected.add(MoveDirection.BACKWARD);
        expected.add(MoveDirection.LEFT);
        expected.add(MoveDirection.RIGHT);

        List<MoveDirection> out = OptionsParser.parse(in);

        assertIterableEquals(expected, out);
    }

    @Test
    void parseWordsAndLettersTest() {
        String[] in = {
                "forward",
                "backward",
                "left",
                "right",
                "f",
                "b",
                "l",
                "r"
        };

        List<MoveDirection> expected = new LinkedList<>();
        expected.add(MoveDirection.FORWARD);
        expected.add(MoveDirection.BACKWARD);
        expected.add(MoveDirection.LEFT);
        expected.add(MoveDirection.RIGHT);
        expected.add(MoveDirection.FORWARD);
        expected.add(MoveDirection.BACKWARD);
        expected.add(MoveDirection.LEFT);
        expected.add(MoveDirection.RIGHT);

        List<MoveDirection> out = OptionsParser.parse(in);

        assertIterableEquals(expected, out);
    }

    @Test
    void parseWithWrongData() {
        String[] in = {
                "for",
                "forward",
                "backward",
                "left",
                "o",
                "right",
                "fds"
        };

        List<MoveDirection> expected = new LinkedList<>();
        expected.add(MoveDirection.FORWARD);
        expected.add(MoveDirection.BACKWARD);
        expected.add(MoveDirection.LEFT);
        expected.add(MoveDirection.RIGHT);

        List<MoveDirection> out = OptionsParser.parse(in);

        assertIterableEquals(expected, out);
    }

    @Test
    void parseWithWrongData2() {
        String[] in = {
                "for",
                "forward",
                "b",
                "r",
                "d",
                "backward",
                "left",
                "f",
                "left",
                "b",
                "right"
        };

        List<MoveDirection> expected = new LinkedList<>();
        expected.add(MoveDirection.FORWARD);
        expected.add(MoveDirection.BACKWARD);
        expected.add(MoveDirection.RIGHT);
        expected.add(MoveDirection.BACKWARD);
        expected.add(MoveDirection.LEFT);
        expected.add(MoveDirection.FORWARD);
        expected.add(MoveDirection.LEFT);
        expected.add(MoveDirection.BACKWARD);
        expected.add(MoveDirection.RIGHT);

        List<MoveDirection> out = OptionsParser.parse(in);

        assertIterableEquals(expected, out);
    }
}