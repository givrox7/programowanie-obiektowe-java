package agh.cs.lab3;

import agh.cs.lab2.Vector2d;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class WorldTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    void mainIntegrationTest() {
        String[] in = {
                "fds",
                "dsa",
                "for",
                "forward",
                "b",
                "r",
                "r",
                "d",
                "backward",
                "left",
                "f",
                "left",
                "f",
                "f",
                "f",
                "right"
        };

        World.main(in);

        String expected = new Vector2d(3, 4).toString() + ">";
        assertEquals(expected, outContent.toString());
    }

    @Test
    void mainDefaultTest() {
        String[] in = {
                "b",
                "r",
                "f",
                "f",
        };

        World.main(in);

        String expected = new Vector2d(4, 1).toString() + ">";
        assertEquals(expected, outContent.toString());
    }

    @Test
    void mainDifferentDataTest() {
        String[] in = {
                "for",
                "forward",
                "b",
                "r",
                "d",
                "backward",
                "left",
                "f",
                "left",
                "b",
                "right"
        };

        World.main(in);

        String expected = new Vector2d(2, 3).toString() + "^";
        assertEquals(expected, outContent.toString());
    }

    @Test
    void mainBorderTest() {
        String[] in = {
                "f",
                "f",
                "f",
                "r",
                "b",
                "b",
                "b"
        };

        World.main(in);

        String expected = new Vector2d(0, 4).toString() + ">";
        assertEquals(expected, outContent.toString());
    }
}