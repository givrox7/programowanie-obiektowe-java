package agh.cs.lab3;

import agh.cs.lab2.MoveDirection;
import agh.cs.lab2.Vector2d;
import agh.cs.lab4.RectangularMap;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AnimalTest {

    @Test
    void constructorTest_Map(){
        Animal animal = new Animal(new RectangularMap(4, 4));
        assertEquals(new Vector2d(2,2), animal.getPosition());
    }

    @Test
    void constructorTest_Position(){
        Animal animal = new Animal(new RectangularMap(4, 4), new Vector2d(1,3));
        assertEquals(new Vector2d(1,3), animal.getPosition());
    }

    @Test
    void moveTest_rotL1() {
        Animal animal = new Animal(new RectangularMap(4, 4));
        animal.move(MoveDirection.LEFT);

        String expected = new Vector2d(2, 2).toString() + "<";
        assertEquals(expected, animal.getPosition() + animal.toString());
    }

    @Test
    void moveTest_rotL2() {
        Animal animal = new Animal(new RectangularMap(4, 4));
        animal.move(MoveDirection.LEFT);
        animal.move(MoveDirection.LEFT);

        String expected = new Vector2d(2, 2).toString() + "v";
        assertEquals(expected, animal.getPosition() + animal.toString());
    }

    @Test
    void moveTest_rotL3() {
        Animal animal = new Animal(new RectangularMap(4, 4));
        animal.move(MoveDirection.LEFT);
        animal.move(MoveDirection.LEFT);
        animal.move(MoveDirection.LEFT);
        animal.move(MoveDirection.LEFT);

        String expected = new Vector2d(2, 2).toString() + "^";
        assertEquals(expected, animal.getPosition() + animal.toString());
    }

    @Test
    void moveTest_rotR1() {
        Animal animal = new Animal(new RectangularMap(4, 4));
        animal.move(MoveDirection.RIGHT);

        String expected = new Vector2d(2, 2).toString() + ">";
        assertEquals(expected, animal.getPosition() + animal.toString());
    }

    @Test
    void moveTest_rotR2() {
        Animal animal = new Animal(new RectangularMap(4, 4));
        animal.move(MoveDirection.RIGHT);
        animal.move(MoveDirection.RIGHT);

        String expected = new Vector2d(2, 2).toString() + "v";
        assertEquals(expected, animal.getPosition() + animal.toString());
    }

    @Test
    void moveTest_rotR3() {
        Animal animal = new Animal(new RectangularMap(4, 4));
        animal.move(MoveDirection.RIGHT);
        animal.move(MoveDirection.RIGHT);
        animal.move(MoveDirection.RIGHT);
        animal.move(MoveDirection.RIGHT);

        String expected = new Vector2d(2, 2).toString() + "^";
        assertEquals(expected, animal.getPosition() + animal.toString());
    }

    @Test
    void moveTest_for1() {
        Animal animal = new Animal(new RectangularMap(4, 4));
        animal.move(MoveDirection.FORWARD);

        String expected = new Vector2d(2, 3).toString() + "^";
        assertEquals(expected, animal.getPosition() + animal.toString());
    }

    @Test
    void moveTest_for2() {
        Animal animal = new Animal(new RectangularMap(4, 4));
        animal.move(MoveDirection.FORWARD);
        animal.move(MoveDirection.FORWARD);

        String expected = new Vector2d(2, 4).toString() + "^";
        assertEquals(expected, animal.getPosition() + animal.toString());
    }

    @Test
    void moveTest_forBorder() {
        Animal animal = new Animal(new RectangularMap(4, 4));
        animal.move(MoveDirection.FORWARD);
        animal.move(MoveDirection.FORWARD);
        animal.move(MoveDirection.FORWARD);

        String expected = new Vector2d(2, 4).toString() + "^";
        assertEquals(expected, animal.getPosition() + animal.toString());
    }

    @Test
    void moveTest_back1() {
        Animal animal = new Animal(new RectangularMap(4, 4));
        animal.move(MoveDirection.BACKWARD);

        String expected = new Vector2d(2, 1).toString() + "^";
        assertEquals(expected, animal.getPosition() + animal.toString());
    }

    @Test
    void moveTest_back2() {
        Animal animal = new Animal(new RectangularMap(4, 4));
        animal.move(MoveDirection.BACKWARD);
        animal.move(MoveDirection.BACKWARD);

        String expected = new Vector2d(2, 0).toString() + "^";
        assertEquals(expected, animal.getPosition() + animal.toString());
    }

    @Test
    void moveTest_backBorder() {
        Animal animal = new Animal(new RectangularMap(4, 4));
        animal.move(MoveDirection.BACKWARD);
        animal.move(MoveDirection.BACKWARD);
        animal.move(MoveDirection.BACKWARD);

        String expected = new Vector2d(2, 0).toString() + "^";
        assertEquals(expected, animal.getPosition() + animal.toString());
    }

    @Test
    void moveTest_mix1() {
        Animal animal = new Animal(new RectangularMap(4, 4));
        animal.move(MoveDirection.RIGHT);
        animal.move(MoveDirection.FORWARD);

        String expected = new Vector2d(3, 2).toString() + ">";
        assertEquals(expected, animal.getPosition() + animal.toString());
    }

    @Test
    void moveTest_RBorder() {
        Animal animal = new Animal(new RectangularMap(4, 4));
        animal.move(MoveDirection.RIGHT);
        animal.move(MoveDirection.FORWARD);
        animal.move(MoveDirection.FORWARD);
        animal.move(MoveDirection.FORWARD);

        String expected = new Vector2d(4, 2).toString() + ">";
        assertEquals(expected, animal.getPosition() + animal.toString());
    }

    @Test
    void moveTest_mix2() {
        Animal animal = new Animal(new RectangularMap(4, 4));
        animal.move(MoveDirection.LEFT);
        animal.move(MoveDirection.FORWARD);

        String expected = new Vector2d(1, 2).toString() + "<";
        assertEquals(expected, animal.getPosition() + animal.toString());
    }

    @Test
    void moveTest_LBorder() {
        Animal animal = new Animal(new RectangularMap(4, 4));
        animal.move(MoveDirection.LEFT);
        animal.move(MoveDirection.FORWARD);
        animal.move(MoveDirection.FORWARD);
        animal.move(MoveDirection.FORWARD);

        String expected = new Vector2d(0, 2).toString() + "<";
        assertEquals(expected, animal.getPosition() + animal.toString());
    }

    @Test
    void moveTest_mix3() {
        Animal animal = new Animal(new RectangularMap(4, 4));
        animal.move(MoveDirection.LEFT);
        animal.move(MoveDirection.FORWARD);
        animal.move(MoveDirection.FORWARD);
        animal.move(MoveDirection.FORWARD);
        animal.move(MoveDirection.RIGHT);
        animal.move(MoveDirection.BACKWARD);
        animal.move(MoveDirection.BACKWARD);
        animal.move(MoveDirection.BACKWARD);
        animal.move(MoveDirection.BACKWARD);

        String expected = new Vector2d(0, 0).toString() + "^";
        assertEquals(expected, animal.getPosition() + animal.toString());
    }
}