package agh.cs.lab6;

import agh.cs.lab2.Vector2d;
import agh.cs.lab3.Animal;
import agh.cs.lab4.RectangularMap;
import agh.cs.lab5.Grass;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.edu.agh.po.lab01.World;

import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class WorldMapFieldTest {
    Animal animal = new Animal(new RectangularMap(2, 2));
    WorldMapField field = new WorldMapField();
    Grass grass = new Grass(new Vector2d(2, 2));

    @Test
    void viewOrderConstanceTest() {
        Map<Class<? extends IMapElement>, Integer> order = WorldMapField.getViewOrder();
        int lastSize = order.size();
        abstract class local implements IMapElement{}
        try {
            order.put(local.class, 0);
        }catch(Exception ignored){}
        assertEquals(lastSize, WorldMapField.getViewOrder().size());
    }

    @Test
    void isObstacleTest() {
        field.place(animal);
        assertTrue(field.isObstacle());
    }

    @Test
    void isNotObstacleTest() {
        field.place(grass);
        assertFalse(field.isObstacle());
    }

    @Test
    void popReturnTest() {
        field.place(animal);
        assertSame(animal, field.pop(animal).get());
    }

    @Test
    void popResultTest() {
        field.place(animal);
        field.pop(animal);
        assertSame(Optional.empty(), field.top());
    }

    @Test
    void popManyObjectsResult1() {
        field.place(animal);
        field.place(grass);
        field.pop(animal);
        assertSame(grass, field.top().get());
    }

    @Test
    void popManyObjectsResult2() {
        field.place(animal);
        field.place(grass);
        field.pop(grass);
        assertSame(animal, field.top().get());
    }

    @Test
    void placeOrderTest1(){
        field.place(grass);
        field.place(animal);
        assertSame(animal, field.top().get());
    }

    @Test
    void placeOrderTest2(){
        field.place(animal);
        field.place(grass);
        assertSame(animal, field.top().get());
    }
}