package agh.cs.lab5;

import agh.cs.lab2.MoveDirection;
import agh.cs.lab2.Vector2d;
import agh.cs.lab3.Animal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class GrassFieldTest extends AbstractWorldMapTest<GrassField>{

    @BeforeEach
    public void instantiateMap() {
        map = new GrassField(2);
    }

    @Test
    public void canMoveTo_EmptyOut() {
        assertTrue(map.canMoveTo(new Vector2d(2,5)));
    }

    @Test
    public void placeOut() {
        assertTrue(map.place(new Animal(map, new Vector2d(0,5))));
    }

    @Test
    public void run1AnimalBorders() {
        Animal animal = new Animal(map, new Vector2d(2,2));
        map.place(animal);
        List<MoveDirection> directions = new LinkedList<>();

        directions.add(MoveDirection.FORWARD);
        directions.add(MoveDirection.RIGHT);
        directions.add(MoveDirection.FORWARD);
        directions.add(MoveDirection.FORWARD);
        directions.add(MoveDirection.FORWARD);

        map.run(directions);

        String expected = new Vector2d(5, 3).toString() + ">";
        assertEquals(expected, animal.getPosition() + animal.toString());
    }
}