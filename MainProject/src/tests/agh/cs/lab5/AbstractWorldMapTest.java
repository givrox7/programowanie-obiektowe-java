package agh.cs.lab5;

import agh.cs.lab2.MoveDirection;
import agh.cs.lab2.Vector2d;
import agh.cs.lab3.Animal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public abstract class AbstractWorldMapTest<T extends AbstractWorldMap> {
    protected T map;

    @BeforeEach
    public abstract void instantiateMap();

    @Test
    void canMoveTo_Empty() {
        assertTrue(map.canMoveTo(new Vector2d(2,2)));
    }

    @Test
    void canMoveTo_EmptyBorder1() {
        assertTrue(map.canMoveTo(new Vector2d(3,4)));
    }

    @Test
    void canMoveTo_EmptyBorder2() {
        assertTrue(map.canMoveTo(new Vector2d(0,2)));
    }

    @Test
    void canMoveTo_Occupied1() {
        map.place(new Animal(map, new Vector2d(3,1)));
        assertFalse(map.canMoveTo(new Vector2d(3,1)));
    }

    @Test
    void canMoveTo_NearOccupied1() {
        map.place(new Animal(map, new Vector2d(3,1)));
        assertTrue(map.canMoveTo(new Vector2d(3,2)));
    }

    @Test
    void placeEmpty() {
        assertTrue(map.place(new Animal(map, new Vector2d(3,1))));
    }

    @Test
    void placeOccupied() {
        map.place(new Animal(map, new Vector2d(3,1)));
        assertThrows(IllegalArgumentException.class, () -> map.place(new Animal(map, new Vector2d(3,1))));
    }

    @Test
    void runEmptyMap() {
        List<MoveDirection> directions = new LinkedList<>();
        directions.add(MoveDirection.FORWARD);
        directions.add(MoveDirection.RIGHT);
        directions.add(MoveDirection.FORWARD);

        assertTimeoutPreemptively(Duration.ofMillis(1000), () -> map.run(directions));
    }

    @Test
    void run1Animal() {
        Animal animal = new Animal(map, new Vector2d(2,2));
        map.place(animal);
        List<MoveDirection> directions = new LinkedList<>();

        directions.add(MoveDirection.FORWARD);
        directions.add(MoveDirection.RIGHT);
        directions.add(MoveDirection.FORWARD);

        map.run(directions);

        String expected = new Vector2d(3, 3).toString() + ">";
        assertEquals(expected, animal.getPosition() + animal.toString());
    }

    @Test
    void run2AnimalCollision() {
        Animal animal1 = new Animal(map, new Vector2d(2,1));
        Animal animal2 = new Animal(map, new Vector2d(1,2));
        map.place(animal1);
        map.place(animal2);
        List<MoveDirection> directions = new LinkedList<>();

        directions.add(MoveDirection.FORWARD);
        directions.add(MoveDirection.RIGHT);
        directions.add(MoveDirection.LEFT);
        directions.add(MoveDirection.FORWARD);

        map.run(directions);

        String expected = new Vector2d(2, 2).toString() + "<" + new Vector2d(1, 2)+ ">";
        String actual = animal1.getPosition() + animal1.toString() + animal2.getPosition() + animal2.toString();
        assertEquals(expected, actual);
    }

    @Test
    void objectAt_Occupied() {
        Animal animal = new Animal(map, new Vector2d(3,0));
        map.place(animal);
        assertSame(animal, map.objectAt(new Vector2d(3,0)).orElse(null));
    }

    @Test
    void objectAt_Out() {
        assertSame(Optional.empty(), map.objectAt(new Vector2d(1,5)));
    }
}