package agh.cs.lab7;

import agh.cs.lab2.MoveDirection;
import agh.cs.lab2.Vector2d;
import agh.cs.lab3.Animal;
import agh.cs.lab4.RectangularMap;
import agh.cs.lab6.IMapElement;
import agh.cs.lab6.WorldMapField;
import pl.edu.agh.po.lab04.IWorldMap;

import java.util.Map;
import java.util.TreeSet;

public class TestWorld {
    public static void main(String[] args) {
        try {
            TreeSet<IMapElement> OX = new TreeSet<>((IMapElement o1, IMapElement o2)->{
                if(o1.getPosition().x != o2.getPosition().x)
                    return Integer.compare(o1.getPosition().x, o2.getPosition().x);
                if(o1.getPosition().y != o2.getPosition().y)
                    return Integer.compare(o1.getPosition().y, o2.getPosition().y);
                Map<Class<? extends IMapElement>, Integer> viewOrder = WorldMapField.getViewOrder();
                return Integer.compare(viewOrder.get(o1.getClass()), viewOrder.get(o2.getClass()));
            });
            IWorldMap map = new RectangularMap(4, 4);
            Animal animal1 = new Animal(map, new Vector2d(2, 2));
            Animal animal2 = new Animal(map, new Vector2d(2, 1));
            OX.add(animal1);
            OX.add(animal2);

            System.out.println(OX.size());

            OX.remove(animal2);

            System.out.println(OX.size());

            OX.add(animal2);

            System.out.println(OX.size());

            animal2.move(MoveDirection.FORWARD);
            System.out.println(OX.remove(animal2));

            System.out.println(OX.first().getPosition());

            OX.add(animal2);

            System.out.println(OX.size());


        }catch(Exception exception) {
            System.out.println(exception.toString());
        }
    }
}
