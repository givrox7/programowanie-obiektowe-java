package agh.cs.lab7;

import agh.cs.lab2.MoveDirection;
import agh.cs.lab2.Vector2d;
import agh.cs.lab3.Animal;
import agh.cs.lab4.RectangularMap;
import agh.cs.lab5.Grass;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.edu.agh.po.lab04.IWorldMap;

import static org.junit.jupiter.api.Assertions.*;

class MapBoundaryTest {
    MapBoundary mapBoundary;
    IWorldMap rectangularMap;
    Animal animal1;
    Animal animal2;
    Animal animal3;
    Grass grass1;
    Grass grass2;

    @BeforeEach
    void init() {
        mapBoundary = new MapBoundary();
        rectangularMap = new RectangularMap(10,5);
        animal1 = new Animal(rectangularMap, new Vector2d(2, 2));
        animal2 = new Animal(rectangularMap, new Vector2d(3, 4));
        animal3 = new Animal(rectangularMap, new Vector2d(3, 1));
        grass1 = new Grass(new Vector2d(2, 0));
        grass2 = new Grass(new Vector2d(1, 3));
    }

    @Test
    void getBottomLeft() {
        mapBoundary.place(grass1);
        mapBoundary.place(grass2);
        assertEquals(new Vector2d(1, 0), mapBoundary.getBottomLeft());
    }

    @Test
    void getTopRight() {
        mapBoundary.place(grass1);
        mapBoundary.place(grass2);
        assertEquals(new Vector2d(2, 3), mapBoundary.getTopRight());
    }

    @Test
    void getTopRightEmpty(){
        assertEquals(new Vector2d(0, 0), mapBoundary.getTopRight());
    }

    @Test
    void getBottomLeftEmpty(){
        assertEquals(new Vector2d(0, 0), mapBoundary.getBottomLeft());
    }

    @Test
    void placeManyObjectsBot(){
        mapBoundary.place(new Animal(rectangularMap, new Vector2d(1, 2)));
        mapBoundary.place(new Grass(new Vector2d(1, 2)));
        mapBoundary.place(new Grass(new Vector2d(3, 1)));
        mapBoundary.place(new Grass(new Vector2d(1, 4)));
        mapBoundary.place(new Grass(new Vector2d(1, 2)));
        mapBoundary.place(new Grass(new Vector2d(8, 2)));
        mapBoundary.place(new Grass(new Vector2d(1, 1)));
        mapBoundary.place(new Animal(rectangularMap, new Vector2d(3, 2)));
        mapBoundary.place(new Animal(rectangularMap, new Vector2d(1, 0)));
        mapBoundary.place(new Animal(rectangularMap, new Vector2d(0, 1)));
        assertEquals(new Vector2d(0,0), mapBoundary.getBottomLeft());
    }

    @Test
    void placeManyObjectsTop(){
        mapBoundary.place(new Animal(rectangularMap, new Vector2d(1, 2)));
        mapBoundary.place(new Grass(new Vector2d(1, 2)));
        mapBoundary.place(new Grass(new Vector2d(3, 1)));
        mapBoundary.place(new Grass(new Vector2d(1, 4)));
        mapBoundary.place(new Grass(new Vector2d(1, 2)));
        mapBoundary.place(new Grass(new Vector2d(8, 2)));
        mapBoundary.place(new Grass(new Vector2d(1, 1)));
        mapBoundary.place(new Animal(rectangularMap, new Vector2d(3, 2)));
        mapBoundary.place(new Animal(rectangularMap, new Vector2d(1, 0)));
        mapBoundary.place(new Animal(rectangularMap, new Vector2d(0, 1)));
        assertEquals(new Vector2d(8,4), mapBoundary.getTopRight());
    }

    @Test
    void place2AnimalsBot(){
        mapBoundary.place(animal1);
        mapBoundary.place(animal3);
        assertEquals(new Vector2d(2, 1), mapBoundary.getBottomLeft());
    }

    @Test
    void place2AnimalsTop(){
        mapBoundary.place(animal1);
        mapBoundary.place(animal3);
        assertEquals(new Vector2d(3, 2), mapBoundary.getTopRight());
    }

    @Test
    void remove() {
        mapBoundary.place(animal1);
        mapBoundary.place(animal3);
        mapBoundary.remove(animal3);
        assertEquals(new Vector2d(2, 2), mapBoundary.getTopRight());
    }

    @Test
    void removeAndPlace() {
        mapBoundary.place(animal1);
        mapBoundary.place(animal3);
        mapBoundary.remove(animal3);
        mapBoundary.place(animal3);
        assertEquals(new Vector2d(3, 2), mapBoundary.getTopRight());
    }

    @Test
    void positionChanged() {
        mapBoundary.place(grass1);
        mapBoundary.place(grass2);
        mapBoundary.place(animal1);
        rectangularMap.place(animal1);
        animal1.move(MoveDirection.RIGHT);
        animal1.move(MoveDirection.FORWARD);
        assertEquals(new Vector2d(3, 3), mapBoundary.getTopRight());
    }

    @Test
    void positionChangedBack() {
        mapBoundary.place(grass1);
        mapBoundary.place(grass2);
        mapBoundary.place(animal1);
        rectangularMap.place(animal1);
        animal1.move(MoveDirection.RIGHT);
        animal1.move(MoveDirection.FORWARD);
        animal1.move(MoveDirection.BACKWARD);
        assertEquals(new Vector2d(2, 3), mapBoundary.getTopRight());
    }

    @Test
    void positionChangedOneRow() {
        rectangularMap.place(animal1);
        rectangularMap.place(animal3);
        mapBoundary.place(animal1);
        mapBoundary.place(animal3);
        animal3.move(MoveDirection.FORWARD);
        animal3.move(MoveDirection.FORWARD);

        assertEquals(new Vector2d(2, 2), mapBoundary.getBottomLeft());
        assertEquals(new Vector2d(3, 3), mapBoundary.getTopRight());
    }

    @Test
    void positionChangedOneRowBack() {
        rectangularMap.place(animal1);
        rectangularMap.place(animal2);
        mapBoundary.place(animal1);
        mapBoundary.place(animal2);
        animal1.move(MoveDirection.FORWARD);
        animal2.move(MoveDirection.BACKWARD);
        animal1.move(MoveDirection.RIGHT);
        animal2.move(MoveDirection.BACKWARD);

        assertEquals(new Vector2d(2, 2), mapBoundary.getBottomLeft());
        assertEquals(new Vector2d(3, 3), mapBoundary.getTopRight());
    }

    @Test
    void positionChangedLong() {
        rectangularMap.place(animal1);
        rectangularMap.place(animal2);
        mapBoundary.place(animal1);
        mapBoundary.place(animal2);
        animal1.move(MoveDirection.FORWARD);
        animal2.move(MoveDirection.BACKWARD);
        animal1.move(MoveDirection.RIGHT);
        animal2.move(MoveDirection.BACKWARD);
        //animal1.move(MoveDirection.LEFT);
        //animal2.move(MoveDirection.FORWARD);
        //animal1.move(MoveDirection.LEFT);
        //animal2.move(MoveDirection.BACKWARD);
        //animal1.move(MoveDirection.RIGHT);
        assertEquals(new Vector2d(2, 2), mapBoundary.getBottomLeft());
        assertEquals(new Vector2d(3, 3), mapBoundary.getTopRight());

    }
}