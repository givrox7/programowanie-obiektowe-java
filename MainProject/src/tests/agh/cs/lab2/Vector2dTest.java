package agh.cs.lab2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Vector2dTest {

    @Test
    void precedes() {
        Vector2d v1 = new Vector2d(5, -3);
        Vector2d v2 = new Vector2d(5, 3);
        Vector2d v3 = new Vector2d(0, 2);
        assertTrue(v1.precedes(v2));
        assertFalse(v1.precedes(v3));
        assertFalse(v3.precedes(v1));
        assertFalse(v2.precedes(v3));
        assertTrue(v3.precedes(v2));
    }

    @Test
    void follows() {
        Vector2d v1 = new Vector2d(5, -3);
        Vector2d v2 = new Vector2d(5, 3);
        Vector2d v3 = new Vector2d(0, 2);
        assertFalse(v1.follows(v2));
        assertFalse(v1.follows(v3));
        assertFalse(v3.follows(v1));
        assertTrue(v2.follows(v3));
        assertFalse(v3.follows(v2));
    }

    @Test
    void upperRight() {
        Vector2d v1 = new Vector2d(5, -3);
        Vector2d v2 = new Vector2d(5, 3);
        Vector2d v3 = new Vector2d(-1, 3);
        Vector2d v4 = new Vector2d(-1, -3);
        assertEquals(v1.upperRight(v2), v2);
        assertEquals(v1.upperRight(v3), v2);
        assertEquals(v1.upperRight(v4), v1);
        assertEquals(v4.upperRight(v2), v2);
        assertEquals(v3.upperRight(v3), v3);

    }

    @Test
    void lowerLeft() {
        Vector2d v1 = new Vector2d(5, -3);
        Vector2d v2 = new Vector2d(5, 3);
        Vector2d v3 = new Vector2d(-1, 3);
        Vector2d v4 = new Vector2d(-1, -3);
        assertEquals(v1.lowerLeft(v2), v1);
        assertEquals(v1.lowerLeft(v4), v4);
        assertEquals(v1.lowerLeft(v3), v4);
        assertEquals(v4.lowerLeft(v2), v4);
        assertEquals(v3.lowerLeft(v3), v3);
    }

    @Test
    void add() {
        Vector2d v1 = new Vector2d(5, -3);
        Vector2d v2 = new Vector2d(5, 3);
        Vector2d v3 = new Vector2d(-1, 3);
        Vector2d v4 = new Vector2d(-1, -3);
        assertEquals(v1.add(v2), new Vector2d(10, 0));
        assertEquals(v4.add(v2), new Vector2d(4, 0));
        assertEquals(v4.add(v3), new Vector2d(-2, 0));
        assertEquals(v4.add(v1), new Vector2d(4, -6));
        assertEquals(v3.add(v2), new Vector2d(4, 6));
    }

    @Test
    void subtract() {
        Vector2d v1 = new Vector2d(5, -3);
        Vector2d v2 = new Vector2d(5, 3);
        assertEquals(v1.subtract(v2), new Vector2d(0, -6));
        assertEquals(v2.subtract(v1), new Vector2d(0, 6));
    }

    @Test
    void opposite() {
        Vector2d v1 = new Vector2d(5, -3);
        Vector2d v2 = new Vector2d(0, 3);
        assertEquals(v1.opposite(), new Vector2d(-5, 3));
        assertEquals(v2.opposite(), new Vector2d(0, -3));
    }

    @Test
    void testToString() {
        Vector2d v1 = new Vector2d(5, -3);
        assertEquals(v1.toString(), "(5,-3)");
    }

    @Test
    void testEquals() {
        Vector2d v1 = new Vector2d(5, -3);
        Vector2d v2 = new Vector2d(5, -3);
        Vector2d v3 = new Vector2d(-5, -3);
        assertTrue(v1.equals(v2));
        assertTrue(v2.equals(v1));
        assertFalse(v2.equals(v3));
        assertFalse(v3.equals(v2));
    }
}