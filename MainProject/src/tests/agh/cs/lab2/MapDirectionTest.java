package agh.cs.lab2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MapDirectionTest {

    @Test
    void next() {
        MapDirection v = MapDirection.EAST;
        assertSame(v.next(), MapDirection.SOUTH);

        v = MapDirection.SOUTH;
        assertSame(v.next(), MapDirection.WEST);

        v = MapDirection.WEST;
        assertSame(v.next(), MapDirection.NORTH);

        v = MapDirection.NORTH;
        assertSame(v.next(), MapDirection.EAST);
    }

    @Test
    void previous() {
        MapDirection v = MapDirection.EAST;
        assertSame(v.previous(), MapDirection.NORTH);

        v = MapDirection.NORTH;
        assertSame(v.previous(), MapDirection.WEST);

        v = MapDirection.WEST;
        assertSame(v.previous(), MapDirection.SOUTH);

        v = MapDirection.SOUTH;
        assertSame(v.previous(), MapDirection.EAST);
    }
}